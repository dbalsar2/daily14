console.log('entered main.js')

var sendButton = document.getElementById('send-button')
sendButton.onmouseup = getFormInfo()

function getFormInfo(){
	console.log('entered getFormINfo!');

	var host = document.getElementById('select-server-address').selectedIndex;
	var url = document.getElementById('select-server-address').options[host].value;
	var port = document.getElementById('input-port-number').value;

	var method;
	if(document.getElementById("radio-get").checked) {
		method = "GET";
	}
	else if(document.getElementById("radio-put").checked){
		method = "PUT";
	}
	else if(document.getElementById("radio-post").checked){
		method = "POST";
	}
	else if(document.getElementById("radio-delete").checked){
		method = "DELETE";
	}

	var use_key = false;
	var use_body = false;

	if(document.getElementById('checkbox-use-key').checked){
		use_key = true;
	}
	if(document.getElementById('checkbox-use-message').checked){
		use_body = true;
	}

	var key = document.getElementById("input-key").value;
	var body = document.getElementById("text-message-body").value;

	makeServerCall(port, url, method, key, use_key, body, use_body);
}

function makeServerCall(port, url, method, key, use_key, body, use_body){

	var xhr = new XMLHttpRequest();

	if(use_key){
		fullUrl = url + ":" + port + "/movies/" + key;
	}
	else{
		fullUrl = url + ":" + port + "/movies/";
	}

	xhr.open(method, fullUrl, true);

	xhr.onload = function(e) {
		console.log(xhr.responseText);
		showResponse(xhr.responseText, method, key);
	}

	xhr.onerror = function(e) {
		console.error(xhr.statusText);
	}

	if (use_body){
		console.log(message);
		xhr.send(message);
	}
	else{
		xhr.send(null);
	}
}

function showResponse(responseText, method, use_key) {
	console.log("showing response...");

	var label = document.getElementById("response-label");
	var label2 = document.getElementById("answer-label");

	response = JSON.parse(responseText);

	if(method == "GET" ){
	   if (use_key && response["result"] == "success") {
	   		var title = response["title"];
			var genre = response["genres"];
			label2.innerHTML = title + " has the following genre(s): " + genre;
		}
	   else{
		   label2.innerHTML = "-";
		}
	}
	else{
		label2.innerHTML = "-";
	}
	label.innerHTML = responseText;
}
